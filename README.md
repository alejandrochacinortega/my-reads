# Project Instructions

My Reads

## Getting started

`cd` into the my-reads folder and run:

- `yarn install`
- `yarn start`

Open `http://localhost:3000/` and start managing your books.

### Pages ✅

- Main Page (`App.ts`)
- Search (`Search.ts`)

### HTML, CSS and TS ✅

### Documentation ✅

- ReadME
- Comments have been added where needed
- Code quality
