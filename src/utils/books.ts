export const orderBooksByShelf = (books: any[]) => {
  const currentlyReading: any[] = [];
  const wantToRead: any[] = [];
  const read: any[] = [];

  // eslint-disable-next-line
  books.map((book: any) => {
    switch (book.shelf.toUpperCase()) {
      case "CURRENTLYREADING":
        currentlyReading.push(book);
        break;
      case "WANTTOREAD":
        wantToRead.push(book);
        break;
      case "READ":
        read.push(book);
        break;

      default:
        break;
    }
  });

  return {
    currentlyReading,
    wantToRead,
    read,
  };
};
