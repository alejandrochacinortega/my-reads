import React from "react";
import { orderBooksByShelf } from "./utils/books";

import BooksAPI from "./BooksAPI";
import { Link } from "react-router-dom";

class BooksApp extends React.Component {
  state = {
    showSearchPage: false,
    currentlyReading: [],
    books: [],
    wantToRead: [],
    read: [],
    none: [],
  };

  componentDidMount = async () => {
    const books = await BooksAPI.getAll();
    const { currentlyReading, wantToRead, read } = orderBooksByShelf(books);
    this.setState({
      books,
      currentlyReading,
      wantToRead,
      read,
    });
  };

  onChangeSelect = async (event: any, book: any) => {
    try {
      const newShelf = event.target.value;
      const currentShelf = book.shelf;
      await BooksAPI.update(book, newShelf);
      const newBook = await BooksAPI.get(book.id);
      const books = await BooksAPI.getAll();

      this.setState((prevState: any) => {
        return {
          ...prevState,
          [currentShelf]: prevState[currentShelf].filter(
            (b: any) => b.id !== book.id
          ),
          [newShelf]: prevState[newShelf].concat(newBook),
          books,
        };
      });
    } catch (error) {
      console.log("Error updating book ", error);
    }
  };

  renderShelf = (name: string, shelf: any) => {
    return (
      <div className="bookshelf">
        <h2 className="bookshelf-title">{name}</h2>
        <div className="bookshelf-books">
          <ol className="books-grid">
            {shelf.map((book: any) => {
              return (
                <li key={book.id}>
                  <div className="book">
                    <div className="book-top">
                      <div
                        className="book-cover"
                        style={{
                          width: 128,
                          height: 193,
                          backgroundImage:
                            book.imageLinks &&
                            book.imageLinks.thumbnail &&
                            `url(${book.imageLinks.thumbnail})`,
                        }}
                      ></div>
                      <div className="book-shelf-changer">
                        <select
                          defaultValue={book.shelf}
                          onChange={(event) => this.onChangeSelect(event, book)}
                        >
                          <option value="move" disabled>
                            Move to...
                          </option>
                          <option value="currentlyReading">
                            Currently Reading
                          </option>
                          <option value="wantToRead">Want to Read</option>
                          <option value="read">Read</option>
                          <option value="none">None</option>
                        </select>
                      </div>
                    </div>
                    <div className="book-title">{book.title}</div>
                    <div className="book-authors">
                      {book.authors && book.authors.length > 1
                        ? book.authors[0]
                        : book.authors}
                    </div>
                  </div>
                </li>
              );
            })}
          </ol>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="app">
        <div className="list-books">
          <div className="list-books-title">
            <h1>My Reads Project</h1>
          </div>
          <div className="list-books-content">
            {this.renderShelf("Currently Reading", this.state.currentlyReading)}
            {this.renderShelf("Want to read", this.state.wantToRead)}
            {this.renderShelf("Read", this.state.read)}
          </div>
          <div className="open-search">
            <Link to="/search">
              <button>Add a book</button>
            </Link>
          </div>
        </div>
        )}
      </div>
    );
  }
}

export default BooksApp;
