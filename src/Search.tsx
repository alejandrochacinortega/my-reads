import React from "react";
import _ from "lodash";
// import * as BooksAPI from './BooksAPI'
import "./App.css";

import BooksAPI from "./BooksAPI";
import { Link } from "react-router-dom";

class Search extends React.Component {
  state = {
    /**
     * TODO: Instead of using this state variable to keep track of which page
     * we're on, use the URL in the browser's address bar. This will ensure that
     * users can use the browser's back and forward buttons to navigate between
     * pages, as well as provide a good URL they can bookmark and share.
     */
    // showSearchPage: false,
    books: [],
    booksResults: {
      error: "",
      items: [],
      message: "",
    },
  };

  componentDidMount = async () => {
    const books = await BooksAPI.getAll();
    this.setState({
      books,
    });
  };
  onChangeSelectSearch = async (event: any, book: any) => {
    try {
      const newShelf = event.target.value;
      await BooksAPI.update(book, newShelf);
    } catch (error) {
      console.log("Error updating book ", error);
    }
  };

  getDefaultValue = (book: any) => {
    const { books } = this.state;
    const bookInShelf: any = _.find(books, { id: book.id });

    if (bookInShelf) {
      return bookInShelf.shelf;
    }
    return "none";
  };

  renderResults = (booksResults: any) => {
    if (booksResults.items === 0) return;

    return (
      <div className="bookshelf-books">
        <ol className="books-grid">
          {booksResults.items.map((book: any) => {
            const getDefaultValue = this.getDefaultValue(book);
            return (
              <li key={book.id}>
                <div className="book">
                  <div className="book-top">
                    <div
                      className="book-cover"
                      style={{
                        width: 128,
                        height: 193,
                        backgroundImage:
                          book.imageLinks &&
                          book.imageLinks.thumbnail &&
                          `url(${book.imageLinks.thumbnail})`,
                      }}
                    ></div>
                    <div className="book-shelf-changer">
                      <select
                        defaultValue={getDefaultValue}
                        onChange={(event) =>
                          this.onChangeSelectSearch(event, book)
                        }
                      >
                        <option value="move" disabled>
                          Move to...
                        </option>
                        <option value="currentlyReading">
                          Currently Reading
                        </option>
                        <option value="wantToRead">Want to Read</option>
                        <option value="read">Read</option>
                        <option value="none">None</option>
                      </select>
                    </div>
                  </div>
                  <div className="book-title">{book.title}</div>
                  <div className="book-authors">
                    {book.authors && book.authors.length > 1
                      ? book.authors[0]
                      : book.authors}
                  </div>
                </div>
              </li>
            );
          })}
        </ol>
      </div>
    );
  };

  onChangeSearch = async (event: any) => {
    const value = event.target.value;
    if (value.length === 0) {
      this.setState({
        booksResults: {
          error: "",
          items: [],
          message: "Search for something : )",
        },
      });
      return;
    }

    const booksResults = await BooksAPI.search(value);
    if (booksResults.error) {
      this.setState({
        booksResults: {
          error: booksResults.error,
          items: [],
          message: "We couldnt find anything",
        },
      });
      return;
    }

    if (booksResults.items && booksResults.items.length === 0) {
      this.setState({
        booksResults: {
          error: "We couldnt find anything",
          items: [],
          message: "We couldnt find anything",
        },
      });
      return;
    }

    this.setState({ booksResults: { error: "", items: booksResults } });
  };

  render() {
    return (
      <div className="app">
        <div className="search-books">
          <div className="search-books-bar">
            <Link to="/">
              <button
                className="close-search"
                onClick={() => this.setState({ showSearchPage: false })}
              >
                Close
              </button>
            </Link>
            <div className="search-books-input-wrapper">
              <input
                type="text"
                placeholder="Search by title or author"
                onChange={this.onChangeSearch}
              />
            </div>
          </div>
          <div className="search-books-results">
            <ol className="books-grid">
              {this.state.booksResults.items.length > 0
                ? this.renderResults(this.state.booksResults)
                : this.state.booksResults.message}
            </ol>
          </div>
        </div>
      </div>
    );
  }
}

export default Search;
